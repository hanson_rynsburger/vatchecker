const express = require('express');
const bodyParser = require('body-parser');
const soap = require('soap');
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));

const port = process.env.PORT || 8000;

app.listen(port, () => {
  console.log('We are live on ' + port);
});

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  next();
});

app.get('/test-verify', (req, res) => {
  var url =
    'http://ec.europa.eu/taxation_customs/vies/checkVatTestService.wsdl';

  var args = {
    countryCode: req.query.countryCode,
    vatNumber: req.query.vatNumber,
  };

  soap.createClient(url, function(err, client) {
    client.checkVatTestService.checkVatPort.checkVat(args, function(
      err,
      result,
    ) {
      if (err) {
        res.send(err.message, 400);
      } else {
        res.send(result);
      }
    });
  });
});

app.get('/verify', (req, res) => {
  var url = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';

  var args = {
    countryCode: req.query.countryCode,
    vatNumber: req.query.vatNumber,
  };

  soap.createClient(url, function(err, client) {
    client.checkVatService.checkVatPort.checkVat(args, function(err, result) {
      if (err) {
        res.send(err.message, 400);
      } else {
        res.send(result);
      }
    });
  });
});
