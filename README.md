# VATCHECKER

CORS wrapper for below services

http://ec.europa.eu/taxation_customs/vies/checkVatTestService.wsdl
http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl

## Usage

.../test-verify?countryCode=BE&vatNumber=100
vat-valid
.../test-verify?countryCode=BE&vatNumber=200
vat-invalid
.../test-verify?countryCode=BE&vatNumber=300
service unavailable with 400 error

## Live Usage

.../verify?countryCode=BE&vatNumber=100
